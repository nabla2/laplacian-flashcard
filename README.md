## 前提条件

MacOSX もしくは Windows7/10 の環境で、インターネット接続が可能、かつ、以下のツールが導入済みであること。
(yarnは必須ではありません。npm派の人はお手数ですが下記手順を適宜読み替えてください。)

- git 2.x
- JDK 8.x
- gradle 4.x
- node 8.x
- npm 5.x
- yarn 1.x
- chrome
- docker (推奨)

また、必須ではありませんが、下記のchrome extensionをインストールしておくことを推奨します。
  https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd

## 初回セットアップ手順 (react/spring-boot/h2db プロジェクト)

本プロジェクトをclone後、ターミナルでclone先のディレクトリに移動し、以下のコマンドを実行する。

1. 自動生成を実行

    ```bash
    $ gradle
    ```

2. DBテストデータを投入

    ```bash
    $ (cd database && gradle bootrun)
    ```

3. REST APIサーバを起動

    ```bash
    $ (cd service-api && gradle bootrun)
    ```

4. ブラウザでDBテストデータを確認

      http://localhost:8080/h2-console/

    ```yaml
    JDBC URL : jdbc:h2:file:~/laplacian/laplacian_flashcard/db
    User Name: sa
    Password: (空欄)
    ```

5. ブラウザでswaggerコンソールを確認

    http://localhost:8080/swagger-ui.html

6. (別のターミナルで) SPAクライアントアプリケーションを起動

    ```bash
    $ (cd rest-client && yarn && yarn start)
    ```

7. ブラウザで以下のURLを開く

    http://localhost:3000/



## 初回セットアップ手順 (ionic/spring-boot/h2db プロジェクト)

本プロジェクトをclone後、ターミナルでclone先のディレクトリに移動し、以下のコマンドを実行する。

1. Ionic CLI をインストール

    ```bash
    $ yarn global add ionic cordova
    ```

2. Ionicブランクプロジェクトを作成

    ```bash
    $ ionic start ionic-client blank

    > ? Connect this app to the Ionic Dashboard?
    'No'を選択
    ```

3. 自動生成対象リソースを削除

    ```bash
    $ (cd ionic-client && rm -r package.json src/app src/pages)
    ```

4. コード自動生成

    ```bash
    $ gradle
    ```

5. DBテストデータを投入

    ```bash
    $ (cd database && gradle bootrun)
    ```

6. REST APIサーバを起動

    ```bash
    $ (cd service-api && gradle bootrun)
    ```

7. ブラウザでDBテストデータを確認

      http://localhost:8080/h2-console/

    ```yaml
    JDBC URL : jdbc:h2:file:~/laplacian/laplacian_flashcard/db
    User Name: sa
    Password: (空欄)
    ```

8. ブラウザでswaggerコンソールを確認

    http://localhost:8080/swagger-ui.html

9. (別のターミナルで) SPAクライアントアプリケーションを起動

    ```bash
    $ (cd ionic-client && yarn && ionic serve)
    ```
    ※ ブラウザが自動的に起動し、画面が表示されます。

10. (さらに別のターミナルで) 自動打鍵テストシナリオを実行

    ```bash
    $ (cd ionic-client && yarn && yarn e2e)
    ```
    ※ ブラウザが自動的に起動し、自動テストスクリプトが実行されます。

11. (VisualStudio Codoなどの)Markdownの表示が可能なアプリで下記のテストリポート(*.md)を開く

    ```bash
    $ ls ionic-client/e2e-report
    > card-deck.md
    > images
    ```

12. 下記のコマンドによりsonarqubeのインスタンスを起動する

    ```bash
    $ docker -d --name sonar -p 9000:9000 -p 9092:9092 sonarqube
    ```

13. RestAPIプロジェクトの静的コード検証を行う

     ```bash
    $ (cd service-api && gradle sonarqube)
    ```

14. ブラウザで静的コード検証の実行結果を確認する

    http://localhost:9000/dashboard?id=service-api


## その他

gradleコマンドで生成されるプロジェクトは、build.gradle の下記エントリで定義されています。
各タスク
不要なプロジェクトは適宜コメントアウトして下さい。

```groovy
defaultTasks = [
  'generateResourceBundle',
  'generateRestApiService',
  'generateReactBasedWebClient',
  'generateIonicBasedWebClient',
  'generateDataLoaderH2dbFile',
  'generateAngularE2eScript',
]
```

本ツールがサポートしているgradleタスクの一覧は `gradle tasks` コマンドで確認できます。

```
Laplacian generator tasks
-------------------------
downloadExternalResource - ローカルで管理/生成できない外部資源をビルド時にダウンロードする
generateAngularBasedWebClient - Webクライアント(Angular)を生成
generateAngularE2eScript - 自動操作スクリプトを生成(angular-e2e)
generateCucumberJvmScript - 自動操作スクリプトを生成(cucumber-jvm)
generateDataLoaderH2dbFile - データロードツールを生成(h2db-file)
generateDataLoaderPgDocker - データロードツールを生成(pg-docker)
generateEntity - 各種自動生成の元情報となるモデルクラス(Groovy)を生成する。
generateGenerator - 各種自動生成を行うgradleタスク自体を生成する
generateIonicBasedWebClient - Webクライアント(Ionic)を生成
generateReactBasedWebClient - Webクライアント(React)を生成
generateResourceBundle - 各種メッセージを管理するメッセージバンドルを生成する。
generateRestApiService - Restリソースインタフェースの実装系を生成する
generateRestApiServiceWithDummyImpl - SpringBootベースのRestAPIサービスを生成する(Restリソースの実装は ダミー)
installExcelAddin - モデルの整合性をリアルタイム表示するExcelアドインをローカルインストールする。
```


